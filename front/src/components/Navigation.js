import React from "react";

export default function Navigation() {
  return (
    <header className="app-header">
      <a className="app-header__logo" href="#">
        Project
      </a>
      <nav className="app-header__nav">
        <ul className="app-header__nav-list">
          <li className="app-header__nav-list-item">
            <a href="#">Projects</a>
          </li>
          <li className="app-header__nav-list-item">
            <a href="#">About us</a>
          </li>
          <li className="app-header__nav-list-item">
            <a href="#">Stories</a>
          </li>
          <li className="app-header__nav-list-item">
            <a href="#">Contact</a>
          </li>
        </ul>
      </nav>
      <div className="app-header__nav-menu-button">
        <div className="app-header__nav-menu-button-icon">
          <span />
          <span />
          <span />
        </div>
      </div>
    </header>
  );
}
