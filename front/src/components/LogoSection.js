import React from "react";
import logo from "../assets/images/your-logo-here.png";

export default function LogoSection() {
  return (
    <section
      title="Section Logo"
      description="Description for section logo"
      className="app-section app-section--image-overlay app-section--image-peak"
    >
      <img src={logo} alt="Logo icon" className="app-logo" />
      <h1 className="app-title">
        Your Headline <br /> Here
      </h1>
      <h2 className="app-subtitle">
        Lorem ipsum dolor sit amet, consectetur <br />
        adipiscing elit sed do eiusmod.
      </h2>
    </section>
  );
}
