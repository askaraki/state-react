import React, { useState, useEffect } from "react";

export default function CommunitySection() {
  const [communityList, setCommunityList] = useState([]);

  useEffect(() => {
    fetch("http://localhost:3000/community")
      .then((response) => response.json())
      .then((data) => {
        setCommunityList(data);
      })
      .catch((error) => console.error(error));
  }, []);

  const [isSectionVisible, setIsSectionVisible] = useState(true);

  const toggleSectionVisibility = () => {
    setIsSectionVisible(!isSectionVisible);
  };

  return (
    <div>
      <section className="app-section">
        <div className="app-container-title-button">
          <h2 className="app-title">
          Big Community of <br /> People Like You{" "}
        </h2>
        <button onClick={toggleSectionVisibility} className="app-button"> 
          {isSectionVisible ? "Hide section" : "Show section"}
        </button>
        </div>
        
        {isSectionVisible && (
          <>
            <h3 className="app-subtitle">
              We’re proud of our products, and we’re really excited <br /> when
              we get feedback from our users.
            </h3>

            <div className="app-section__users-list">
              {communityList.map((member) => (
                <div className="card" key={member.id}>
                  <img className="card__img" src={member.avatar} alt="Avatar" />
                  <p className="card__description">
                    Lorem ipsum dolor sit amet, <br /> consectetur adipiscing
                    elit, sed do <br /> eiusmod tempor incididunt ut <br />{" "}
                    labore et dolor. <br />
                    <br />
                  </p>
                  <h3 className="card__full-name">
                    {member.firstName} {member.lastName}
                  </h3>
                  <h3 className="card__company-name">{member.position}</h3>
                </div>
              ))}
            </div>
          </>
        )}
      </section>
    </div>
  );
}
