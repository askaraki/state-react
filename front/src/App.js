import './styles/style.css';
import DescriptionSection from './components/DescriptionSection';
import LogoSection from './components/LogoSection';
import Navigation from './components/Navigation';
import LearnMoreSection from './components/LearnMoreSection';
import Footer from './components/Footer';
import CommunitySection from './components/CommunitySection';
import SubscribeSection from './components/SubscribeSection';

function App() {
  return (
    <main id="app-container">
      <Navigation/>
      <LogoSection/>
      <DescriptionSection/>
      <LearnMoreSection/>
      <SubscribeSection/>
      <CommunitySection/>
      <Footer/>
    </main>
  );
}

export default App;
